#include <iostream>
#include <SFML/Graphics.hpp>

int main() {
    sf::RenderWindow window({1280, 720}, "Hunter", sf::Style::Titlebar | sf::Style::Close);
    sf::Event event;

    window.setFramerateLimit(60);

    while (window.isOpen()) {
        while (window.pollEvent(event)) {
            if (event.type == sf::Event::Closed)
                window.close();
        }
        window.clear();
        window.display();
    }

    return EXIT_SUCCESS;
}
